# General compiler flags.
CXXFLAGS = -std=c++2a -fPIC -Iinclude/

# General linker flags.
LDFLAGS = -shared

# Root directory for build results.
BUILDDIR = build

# Root directory for testing.
TESTDIR = test

# The name of the ouput library.
LIBNAME = engine

ifeq ($(release), 1)
	# Release-specific build configuration.

	BUILDDIR := $(BUILDDIR)/release

	CXXFLAGS += -O3 -DNDEBUG
else
	# Debug-specific build configuration

	BUILDDIR := $(BUILDDIR)/debug

	CXXFLAGS += -O2 -pedantic-errors -Wall -Wextra -Wsign-conversion

	# Avoids empty variables.
	release = 0
endif

ifeq ($(win), 1)
	# Windows-specific build configuration.

	CXX = x86_64-w64-mingw32-g++

	BUILDDIR := $(BUILDDIR)/windows

	LIBNAME := $(LIBNAME).dll

	# Loader flags to create import library.
	LDFLAGS += -Wl,--out-implib,$(BUILDDIR)/$(basename $(LIBNAME))_dll.lib

	# Loader flags for mingw32
	LDFLAGS += -static-libstdc++ -static-libgcc -static -lpthread

	# Compile flags for vulkan headers.
	CXXFLAGS += -I./.wine/drive_c/VulkanSDK/latest/Include
  
	# Loader flags for the vulkan library.
	LDFLAGS += -L.wine/drive_c/VulkanSDK/latest/Lib -lvulkan-1
  
	# Compile flags for GLFW headers.
	CXXFLAGS += -Iexternal/glfw-3.3.4.bin.WIN64/include
  
  	# Loader flags for the GLFW library.
	LDFLAGS += -Lexternal/glfw-3.3.4.bin.WIN64/lib-mingw-w64 -lglfw3 -lgdi32  
else
	# Linux-specific build configuration.

	CXX = g++

	BUILDDIR := $(BUILDDIR)/linux

	LIBNAME := $(LIBNAME).so

	# Loader flags for the vulkan library.
  	LDFLAGS += -lvulkan
  
  	# Loader flags for the GLFW library.
  	LDFLAGS += -lglfw

	# Avoids empty variables.
	win = 0
endif

# Root directory for source files.
SOURCEDIR = src/engine

# Outputs all source files in SOURCEDIR, including in subdirectories.
SOURCEFILES := $(wildcard $(SOURCEDIR)/*/*.cpp)

# .o file location can be changed, but for now just use BUILDDIR.
OBJECTDIR = $(BUILDDIR)

# For each source file, output the equivalent .o file in BUILDDIR.
OBJECTFILES = $(patsubst %.cpp,$(OBJECTDIR)/%.o,$(notdir $(SOURCEFILES)))

$(BUILDDIR)/$(LIBNAME): $(OBJECTFILES)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/*/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: test clean tree log

test: $(BUILDDIR)/$(LIBNAME)
	$(MAKE) -C $(TESTDIR) test win=$(win) release=$(release) LIBNAME=$(LIBNAME)

clean:
	rm -f $(BUILDDIR)/*
	$(MAKE) -C $(TESTDIR) clean win=$(win) release=$(release)

tree:
	./tools/tree.sh

log:
	cat data/logs/log.dat

#pragma once

#include <memory>
#include <string>
#include <filesystem>

class Logger {
	public:
		enum LogSeverity {
			FATAL = 4,
			ERROR = 3,
			WARNING = 2,
			INFO = 1,
			DEBUG = 0
		};

		Logger(std::filesystem::path logFilePath, LogSeverity minimumSeverity);
		~Logger();

		void write(std::string message, LogSeverity severity);

		void stop();

	private:
		class LoggerImpl;
		std::unique_ptr<LoggerImpl> mLogger;
};

#pragma once

#include <engine/core/core.hpp>

#include <memory>

extern Logger *logger;

class Renderer {
	public:
		Renderer();
		Renderer(Logger *logger);
		~Renderer();

	private:
		class RendererImpl;
		std::unique_ptr<RendererImpl> mRenderer;
};

#include <engine/core/core.hpp>
#include <engine/renderer/renderer.hpp>

#include <iostream>
#include <unistd.h>

int main() {
	Logger logger{"../data/logs/log.dat", Logger::LogSeverity::DEBUG};
	sleep(1);

	Renderer renderer(&logger);
}

#include "deviceState.hpp"

#include <vulkan/vulkan.hpp>

#include <iostream>

DeviceState::DeviceState(vk::Instance *_instance, vk::SurfaceKHR *_surface) :
	instance{_instance}, surface{_surface} {
	
	selectPhysicalDevice();
}

bool DeviceState::physicalDeviceSuitable(vk::PhysicalDevice) {
	return true;
}

void DeviceState::selectPhysicalDevice() {
	auto physicalDevices = instance->enumeratePhysicalDevices();
	
	if (physicalDevices.size() == 0) {
		logger->write("Found no Vulkan-capable devices.", logger->FATAL);
		throw std::runtime_error(" ");
	} else {
		std::string str = "Found " + std::to_string(physicalDevices.size()) + " Vulkan-capable device(s):";

		for (auto &physicalDevice : physicalDevices) {
			str += "\n\t" + (std::string)physicalDevice.getProperties().deviceName;
		}

		logger->write(str, logger->DEBUG);
	}
	
	bool suitableDeviceFound = false;
	for (auto &_physicalDevice : physicalDevices) {
		if (physicalDeviceSuitable(_physicalDevice)) {
			physicalDevice = _physicalDevice;
			suitableDeviceFound = true;
			break;
		}
	}

	if (!suitableDeviceFound) {
		logger->write("Found no suitable device.", logger->FATAL);
		throw std::runtime_error(" ");
	} else {
		logger->write("Selected " + (std::string)physicalDevice.getProperties().deviceName + " for use.", logger->DEBUG);
	}

	QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
}

QueueFamilyIndices DeviceState::findQueueFamilies(vk::PhysicalDevice physicalDevice) {
	QueueFamilyIndices indices;

	auto queueFamilies = physicalDevice.getQueueFamilyProperties();

	uint32_t i = 0;
	for (auto &queueFamily : queueFamilies) {
		std::string str = "Found queue with the follow capabilities:";

		auto presentSupport = physicalDevice.getSurfaceSupportKHR(
			static_cast<uint32_t>(i), *surface);

		if (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
			str += " GRAPHICS";
			indices.graphicsFamily = i;
		}

		if (queueFamily.queueFlags & vk::QueueFlagBits::eCompute) {
			str += " COMPUTE";
		}

		if (queueFamily.queueFlags & vk::QueueFlagBits::eTransfer) {
			str += " TRANSFER";
		}

		if (presentSupport) {
			str += " PRESENTATION";
		}

		logger->write(str, logger->DEBUG);
	}

	return indices;
}

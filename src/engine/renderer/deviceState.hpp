#pragma once

#include <engine/renderer/renderer.hpp>

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

struct QueueFamilyIndices {
	uint32_t graphicsFamily;
};

class DeviceState {
	public:
		DeviceState(vk::Instance *instance, vk::SurfaceKHR *surface);

		vk::PhysicalDevice physicalDevice;

	private:
		vk::Instance *instance;

		vk::SurfaceKHR *surface;

		bool physicalDeviceSuitable(vk::PhysicalDevice);
		
		void selectPhysicalDevice();
		
		QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice physicalDevice);
};

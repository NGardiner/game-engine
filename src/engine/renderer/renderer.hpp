#pragma once

#include <engine/renderer/renderer.hpp>

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

#include <GLFW/glfw3.h>

class Renderer::RendererImpl {
	public:
		void initialiseRenderer();
};

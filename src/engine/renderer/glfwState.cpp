#include "glfwState.hpp"

glfwState::glfwState() {
	initialiseWindow();
}

glfwState::~glfwState() {
	glfwTerminate();
}

void glfwState::initialiseWindow() {
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = uniqueWindow(glfwCreateWindow(800, 600, "Vulkan", nullptr, nullptr));
}

std::vector<const char *> glfwState::getRequiredExtensions() {
	uint32_t glfwExtensionCount;
	const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	// Convert to a vector to make adding extensions easier.
	std::vector<const char *> requiredExtensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

	return requiredExtensions;
}

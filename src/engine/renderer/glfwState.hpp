#pragma once

#include <engine/renderer/renderer.hpp>

#include <GLFW/glfw3.h>

#include <vector>

class glfwState {
	public:
		struct windowDestructor {
			void operator()(GLFWwindow *window) {
				glfwDestroyWindow(window);
			}
		};

		typedef std::unique_ptr<GLFWwindow, windowDestructor> uniqueWindow;
		uniqueWindow window;

		glfwState();
		~glfwState();

		std::vector<const char *> getRequiredExtensions();

	private:
		void initialiseWindow();
};

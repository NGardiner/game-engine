#pragma once

#include <engine/renderer/renderer.hpp>

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

#include <GLFW/glfw3.h>

class SurfaceState {
	public:
		SurfaceState(vk::Instance *_instance, GLFWwindow *_window);

		vk::UniqueSurfaceKHR surface;

	private:
		void initialiseSurface();

		vk::Instance *instance;
		GLFWwindow *window;
};

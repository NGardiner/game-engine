#include "instanceState.hpp"

namespace {
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
		void *pUserData) {

		logger->write("Validation Layer: " +
			(std::string)pCallbackData->pMessage, logger->WARNING);

		return VK_FALSE;
	}

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
		auto function = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		
		if (function != nullptr) {
			function(instance, debugMessenger, pAllocator);
		}
	}
}

InstanceState::InstanceState(glfwState *glfwState) {
	initialiseInstance(glfwState);
	initialiseDebugMessenger();
}

InstanceState::~InstanceState() {
	destroyDebugMessenger();
}

bool InstanceState::validationLayersAvailable() {
	uint32_t supportedLayerCount;
	vk::enumerateInstanceLayerProperties(&supportedLayerCount, nullptr);

	std::vector<vk::LayerProperties> supportedLayers(supportedLayerCount);
	vk::enumerateInstanceLayerProperties(&supportedLayerCount, supportedLayers.data());

	for (const char *validationLayer : validationLayers) {
		bool layerAvailable = false;

		for (const auto &supportedLayer : supportedLayers) {
			if (strcmp(validationLayer, supportedLayer.layerName) == 0) {
				layerAvailable = true;
				break;
			}
		}

		if (!layerAvailable) {
			return false;
		}
	}

	return true;
}

void InstanceState::initialiseInstance(glfwState *glfwState) {
	vk::ApplicationInfo applicationInfo {
		.pApplicationName = "Rendering Engine",
		.applicationVersion = 1,
		.pEngineName = "No Engine",
		.engineVersion = 1,
		.apiVersion = VK_API_VERSION_1_0
	};

	uint32_t supportedExtensionCount;
	vk::enumerateInstanceExtensionProperties(nullptr, &supportedExtensionCount, nullptr);
                                                                                              
	std::vector<vk::ExtensionProperties> supportedExtensions(supportedExtensionCount);
	vk::enumerateInstanceExtensionProperties(nullptr, &supportedExtensionCount, supportedExtensions.data());
    
	std::vector<const char *> requiredExtensions = glfwState->getRequiredExtensions();

	if (useValidationLayers && !validationLayersAvailable()) {
		logger->write("One or more of the requested validation layers are not available. Validation layers will be disabled.", logger->WARNING);

		useValidationLayers = false;
	} else if (useValidationLayers) {
		requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	logger->write(std::to_string(supportedExtensionCount) + " supported extensions, " + std::to_string(requiredExtensions.size()) + " required extensions.", logger->DEBUG);

	for (const auto &requiredExtension : requiredExtensions) {
		bool extensionSupported = false;

		for (const auto &supportedExtension : supportedExtensions) {
			if (strcmp(requiredExtension, supportedExtension.extensionName) == 0) {
				extensionSupported = true;
				break;
			}
		}

		if (!extensionSupported) {
			logger->write("One or more required extensions are unavailable.", logger->FATAL);
			throw std::runtime_error("");
		}
	}
	
	vk::DebugUtilsMessengerCreateInfoEXT debugMessengerInfo{
		.messageSeverity =
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		.messageType =
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
		.pfnUserCallback = debugCallback
	};

	vk::InstanceCreateInfo instanceInfo {
		.pNext = (vk::DebugUtilsMessengerCreateInfoEXT *) &debugMessengerInfo,
		.pApplicationInfo = &applicationInfo,
		.enabledLayerCount = useValidationLayers ? static_cast<uint32_t>(validationLayers.size()) : 0,
		.ppEnabledLayerNames = useValidationLayers ? validationLayers.data() : nullptr,
		.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size()),
		.ppEnabledExtensionNames = requiredExtensions.data()
	};

	instance = vk::createInstanceUnique(instanceInfo);
}

void InstanceState::initialiseDebugMessenger() {
	if (!useValidationLayers) return;

	vk::DebugUtilsMessengerCreateInfoEXT debugMessengerInfo{
		.messageSeverity =
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		.messageType =
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
		.pfnUserCallback = debugCallback
	};

	auto dispatchLoader = vk::DispatchLoaderDynamic(*instance, vkGetInstanceProcAddr);
	debugMessenger = instance->createDebugUtilsMessengerEXT(debugMessengerInfo, nullptr, dispatchLoader);
}

// There is likely a way to do this properly using the Dispatch Loader,
// though it currently gives an undefined reference error. For now, just
// destroy it the old-fashioned way.
void InstanceState::destroyDebugMessenger() {
	if (!useValidationLayers) return;

	//auto dispatchLoader = vk::DispatchLoaderDynamic(*instance, vkGetInstanceProcAddr);
	//instance->destroyDebugUtilsMessengerEXT(debugMessenger);
	
	DestroyDebugUtilsMessengerEXT(instance.get(), debugMessenger, nullptr);
}

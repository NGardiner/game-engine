#include "renderer.hpp"

#include <GLFW/glfw3.h>

#include <vulkan/vulkan.hpp>

#include "glfwState.hpp"
#include "instanceState.hpp"
#include "surfaceState.hpp"
#include "deviceState.hpp"

Logger *logger;

Renderer::Renderer() {
	Logger _logger{"../data/logs/render.log", Logger::INFO};
	logger = &_logger;

	mRenderer->initialiseRenderer();
}

Renderer::Renderer(Logger *_logger) : mRenderer{ std::make_unique<RendererImpl>() } {
	logger = _logger;

	mRenderer->initialiseRenderer();
}

Renderer::~Renderer() {

}

void Renderer::RendererImpl::initialiseRenderer() {
	glfwState glfwState;

	InstanceState instanceState(&glfwState);

	SurfaceState surfaceState(&instanceState.instance.get(), glfwState.window.get());

	DeviceState deviceState(&instanceState.instance.get(), &surfaceState.surface.get());
}

#include "surfaceState.hpp"

SurfaceState::SurfaceState(vk::Instance *_instance, GLFWwindow *_window) :
	instance{_instance}, window{_window} {

	initialiseSurface();
}

void SurfaceState::initialiseSurface() {
	VkSurfaceKHR tempSurface;
	VkResult result = glfwCreateWindowSurface(*instance, window, nullptr, &tempSurface);

	if (result != VK_SUCCESS) {
		logger->write("Surface creation failed.", logger->FATAL);
		throw std::runtime_error(" ");
	}

	surface = vk::UniqueSurfaceKHR(tempSurface, *instance);
	logger->write("Surface creation successful.", logger->DEBUG);
}

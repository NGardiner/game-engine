#pragma once

#include <engine/renderer/renderer.hpp>

#include "glfwState.hpp"

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define VULKAN_HPP_NO_NODISCARD_WARNINGS
#include <vulkan/vulkan.hpp>

#include <vector>

class InstanceState {
	public:
		vk::UniqueInstance instance;

		InstanceState(glfwState *glfwState);
		~InstanceState();

	private:
		#ifdef NDEBUG
			bool useValidationLayers = false;
		#else
			bool useValidationLayers = true;
		#endif

		const std::vector<const char *> validationLayers = {
			"VK_LAYER_KHRONOS_validation"
		};

		bool validationLayersAvailable();

		void initialiseInstance(glfwState *glfwState);

		VkDebugUtilsMessengerEXT debugMessenger;

		void initialiseDebugMessenger();
		void destroyDebugMessenger();
};

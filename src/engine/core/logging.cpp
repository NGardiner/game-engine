#include "logging.hpp"

#include <filesystem>
#include <fstream>
#include <cstring>

// Sets the minimum log severity, rotates the logs, and starts the logging thread.
Logger::Logger(std::filesystem::path _logFilePath, LogSeverity _minimumSeverity) : mLogger{std::make_unique<LoggerImpl>()} {
	mLogger->minimumSeverity = _minimumSeverity;
	mLogger->logFilePath = _logFilePath;	
	mLogger->logging = true;

	std::rename(_logFilePath.c_str(), (_logFilePath.string() + ".old").c_str());

	// Create the thread with LoggerImpl::run(), passing the pointer from the unique pointer.
	mLogger->loggingThread = std::thread(&Logger::LoggerImpl::run, mLogger.get());
}

// The destructor needs to be defined here, even if it's empty.
Logger::~Logger() {
	stop();
}

void Logger::write(std::string message, LogSeverity severity) {
	if (mLogger->minimumSeverity > severity) return;

	// Get the current time and format it.
	time_t currentTime = time(0);
	tm *localTime = localtime(&currentTime);
	std::string timeString = std::to_string(localTime->tm_hour) + ":" + std::to_string(localTime->tm_min) + ":" + std::to_string(localTime->tm_sec);

	// Acquire the lock, push the message, and notify the logging thread.
	std::lock_guard<std::mutex> lock(mLogger->queueMutex);
	mLogger->logMessages.push(timeString + " [" + mLogger->severityStrings[severity] + "] - " + message);
	mLogger->condition.notify_all();
}

void Logger::stop() {
	// If the logger has already been stopped, don't try to stop it again.
	if (!mLogger->logging) return;

	// The lock needs to be released before calling .join().
	{
		std::lock_guard<std::mutex> lock(mLogger->queueMutex);
		mLogger->logging = false;
		mLogger->condition.notify_all();
	}

	mLogger->loggingThread.join();
}

void Logger::LoggerImpl::run() {
	std::ofstream fileStream;

	std::filesystem::create_directory(logFilePath.parent_path());
	fileStream.open(logFilePath, std::ios::out | std::ios::trunc);
	fileStream << "--- LOG START ---" << std::endl;

	while (logging) {
		std::unique_lock<std::mutex> lock(queueMutex);
		
		// Wait until the thread is notified, there's messages and the queue, and logging is enabled.
		condition.wait(lock, [this]{ return logMessages.size() > 0 || !logging; });

		// Write every message in the queue.
		while (!logMessages.empty()) {
			fileStream << logMessages.front() << std::endl;
			logMessages.pop();
		}
	}

	fileStream.close();
}

#pragma once

#include <engine/core/core.hpp>

#include <queue>
#include <condition_variable>
#include <mutex>
#include <thread>

class Logger::LoggerImpl {
	public:
		bool logging;

		std::queue<std::string> logMessages;
		std::thread loggingThread;
		std::mutex queueMutex;
		std::condition_variable condition;

		LogSeverity minimumSeverity;
		std::filesystem::path logFilePath;

		void run();

		std::string severityStrings[5] = { "DEBUG", "INFO", "WARNING", "ERROR", "FATAL" };
};
